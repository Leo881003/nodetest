var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express'});
});

router.param('x', function(req, res, next, id) {
    res.locals.x = parseInt(id);
    next();
});

router.param('y', function(req, res, next, id) {
    res.locals.y = parseInt(id);
    next();
});

router.get('/stars/:x/:y', function(req, res) {
    res.render('for', {
        title: '神奇的模板迴圈',
        h: '',
        c: '*'
    });
});

router.get('/jizz/:x/:y', function(req, res) {
    res.render('for', {
        title: '神奇的模板迴圈',
        h: 'ji',
        c: 'z'
    });
});

router.get('/helloworld', function(req, res, next) {
    res.render('helloworld', {});
});

router.get('/vars', function(req, res, next) {
    res.render('vars', { t: '1'});
});

router.get('/plaintexts', function(req, res, next) {
    res.render('plaintexts', {});
});

router.get('/cond_loop', function(req, res, next) {
    res.render('cond_loop', { title: 'cond&loop' });
});

router.get('/html', function(req, res, next) {
    fs.readFile(__dirname + '/../views/html.html', 'utf8', function(err, data){
        if(err) console.log(err);
        res.send(data);
        res.end();
    });
});

router.get('/time', function(req, res, next) {
    res.render('time', { title: 'A Clock' });
});

module.exports = router;
